﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    bool system_online = true;

    public GameObject ship;
    public bool broken_updated = false;
    public bool fixed_updated = true;

    public Light console_light;
    public Light outer_light;
    //public Character character;

    private void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
            Debug.Log(system_online);
        //if (character.isInteracting())

        if (!system_online && !broken_updated)
        {
            ship.GetComponent<ShipSettings>().broken_systems++;
            console_light.color = Color.red;
            outer_light.color = Color.red;
            broken_updated = true;
            fixed_updated = false;
        }
        else if (system_online && !fixed_updated)
        {
            ship.GetComponent<ShipSettings>().broken_systems--;
            console_light.color = Color.cyan;
            outer_light.color = Color.cyan;
            broken_updated = false;
            fixed_updated = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("TRIGGER HAS BEEN ENTERED");
        if (other.gameObject.CompareTag("Player"))
        {
            system_online = !system_online;
        }
    }
}
