﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMe : MonoBehaviour
{
    public GameObject character;    // character to follow
    public float followRadius = 5;  // how far the character can get before we follow them
    public float dampTime = 1;      // parameter for vector smoothing function; higher dampTime => faster camera motion
    public float stopDistance = 1;  // how close we get to the character before stopping;
                                    // if this is zero, we will never stop moving, just slow down
    public bool jumpOnStop = false; // whether or not we set the camera to jump to the character once we get within stopDistance;
                                    // only use if stopDistance is very small.

    // Start is called before the first frame update
    void Start()
    {
        // centre the camera over the character to start with.
        Vector3 target = character.transform.position;
        target.y = transform.position.y; // only move the camera in x and z directions.
        transform.position = target;
    }

    // Update is called once per frame

    bool moving = false;
    Vector3 velocity = Vector3.zero;
    void Update()
    {
        Vector3 destination = character.transform.position;
        destination.y = transform.position.y;
        if (!moving)
        {
            float distance = Vector3.Distance(transform.position, destination);
            if (distance >= followRadius)
            {
                moving = true; // enter 'moving' mode if character is outside boundary
            }
        }
        if (moving)
        {
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime); // use smoothing function
            if (Vector3.Distance(destination, transform.position) <= stopDistance)
            {
                moving = false;
                if (jumpOnStop)
                {
                    transform.position = destination;
                }
            }
        }

    }
}
