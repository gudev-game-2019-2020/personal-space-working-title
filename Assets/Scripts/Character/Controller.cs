﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace controller
{
	public abstract class Controller
	{
		protected Character myChar;



		public Controller(Character c)
		{
			myChar = c;

		}

		public abstract Vector2 get_movement();


		public abstract Vector2 get_direction();


	}


	public class KeyboardController : controller.Controller
	{


		public KeyboardController(Character c) : base(c)
		{

		}



		public override Vector2 get_movement()
		{
			float horizInput = Input.GetAxis("Horizontal");
			float vertiInput = Input.GetAxis("Vertical");


			return new Vector2(horizInput, vertiInput);
		}

		public override Vector2 get_direction()
		{

			Vector3 mousePoint = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
			Vector3 returner = mousePoint - myChar.transform.position;
			returner.y = returner.z;
			//converts the plane from x,y of screen to x,z of top down screen;
			return returner;
		}



		

	}
}
