﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(CharacterController))]
public class Character : MonoBehaviour
{
	public GameObject test;
	controller.Controller controller;
	// the input controller for this character

	[SerializeField]
	private float speed, look_lerp, fov, min_radius, max_radius;


	private CharacterController cc;
	//speed: movement speed of the character
	//look_speed: the speed at which a player rotates towards the desired rotation
	//fov: the angle at which the character can see
	//min_radius: the radius of the small circle surrounding the character
	//max_radius: the maximum distance this character can see in a direction


	// Update is called once per frame

	private void Awake()
	{
		cc = GetComponent<CharacterController>();
		controller = new controller.KeyboardController(this);
	}
	void Update()
    {

		Vector2 movement = controller.get_movement();
		cc.Move(((movement.x * transform.right) + (movement.y * transform.forward)) * Time.deltaTime * speed);

		Vector3 direction = controller.get_direction();

		// The step size is equal to speed times frame time.
		Vector3 newDir = Vector3.Lerp(transform.forward, new Vector3(direction.x, 0, direction.y).normalized, look_lerp * Time.deltaTime);

		// Move our position a step closer to the target.
		transform.rotation = Quaternion.LookRotation(newDir);
		//rotates around the "UP" axis until smoothly towards the desired rotation











		}


	
}
