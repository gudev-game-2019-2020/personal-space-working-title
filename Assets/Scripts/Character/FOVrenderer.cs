﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FOVrenderer : MonoBehaviour
{

	public float resolution = 0.05f, Radius = 12.0f, FOV = 100.0f;
	public int EDF = 16;


	MeshFilter mf;
	Mesh mesh;


	// Start is called before the first frame update
	void Start()
    {

		mf = GetComponent<MeshFilter>();
		mesh = new Mesh();

	}

    // Update is called once per frame
    void Update()
    {
		generate_vision_cone();
    }








	public void generate_vision_cone()
	{

		// this renders the cone of vision for local human players, we don't want the A.I. or networked players to do this, so it should only happen local controllers
		//Mesh mesh = this.GetComponent<MeshFilter>().mesh;
		mesh.Clear();
		// start fresh each frame
		LayerMask lm = LayerMask.GetMask("Walls");
		RaycastHit hit;
		List<Vector3> vertex_positions = new List<Vector3>();
		List<bool> is_hit = new List<bool>();
		for (float i = -0.5f * FOV; i <= 0.5f * FOV; i += FOV * resolution)
		{
			if (Physics.Raycast(transform.position, Quaternion.Euler(Vector3.up * i) * transform.forward, out hit, Radius, lm))
			{
				vertex_positions.Add(hit.point);
				is_hit.Add(true);



			}
			else
			{
				vertex_positions.Add(transform.position + Quaternion.Euler(Vector3.up * i) * transform.forward * Radius);
				is_hit.Add(false);

			}
			//store the vertex positions for the mesh later



		}


		//prune vertices so that only corners and end vertices are used

		for (int i = vertex_positions.Count - 2; i > 0; i--)
		{

			//if (is_hit[i - 1] == is_hit[i] && is_hit[i] == is_hit[i + 1] && Vector3.Angle(vertex_positions[i - 1], vertex_positions[i + 1]) < FOV * resolution * 2f)
			//{

			//unnecesary edge detected
			//vertex_positions.RemoveAt(i);
			//is_hit.RemoveAt(i);
			//}
			//else {

			Debug.DrawLine(transform.position, vertex_positions[i], Color.green);
			if (is_hit[i] != is_hit[i + 1])
			{
				// corner detected: insert new vertex at corner
				Vector3 a = vertex_positions[i];
				Vector3 b = vertex_positions[i + 1];
				Vector3 midpoint = Vector3.Lerp(a, b, 0.5f);
				bool a_hit = is_hit[i];
				bool b_hit = is_hit[i + 1];

				for (int j = 0; j < EDF; j++)
				{
					midpoint = Vector3.Lerp(a, b, 0.5f);
					if (Physics.Raycast(transform.position, midpoint - transform.position, out hit, Radius, lm))
					{
						a = a_hit ? hit.point : a;
						b = !a_hit ? hit.point : b;
						// OK OK bear with me here:
						// if is_hit[a] and is_hit[midpoint] then our boundry has to be between the midpoint between midpoint and b so replace a with the midpoint and run again
						// if !is_hit[a] and is_hit[midpoint] then our boundry must be between the midpoint and b so replace b with the midpoint and run again
					}
					else
					{
						a = !a_hit ? midpoint : a;
						b = a_hit ? midpoint : b;
						// same principle applies here as above ^ just the opposite
					}
				}


				if (a_hit)
				{

					Debug.Log("vertex added");
					vertex_positions.Insert(i + 1, midpoint);
					vertex_positions.Insert(i + 2, transform.position + (Vector3.Lerp(midpoint, b, 0.01f) - transform.position).normalized * Radius);
					Debug.DrawLine(transform.position, midpoint, Color.blue);

				}
				else
				{

					Debug.Log("vertex added");

					vertex_positions.Insert(i + 1, transform.position + (Vector3.Lerp(midpoint, b, 0.01f) - transform.position).normalized * Radius);
					vertex_positions.Insert(i + 2, midpoint);
					Debug.DrawLine(transform.position, midpoint, Color.blue);

				}




				//}
			}

		}

		if (is_hit[0] != is_hit[1])
		{
			// corner detected: insert new vertex at corner
			Vector3 a = vertex_positions[0];
			Vector3 b = vertex_positions[1];
			Vector3 midpoint = Vector3.Lerp(a, b, 0.5f);
			bool a_hit = is_hit[0];
			bool b_hit = is_hit[1];

			for (int j = 0; j < EDF; j++)
			{
				midpoint = Vector3.Lerp(a, b, 0.5f);
				if (Physics.Raycast(transform.position, midpoint - transform.position, out hit, Radius, lm))
				{
					a = a_hit ? hit.point : a;
					b = !a_hit ? hit.point : b;
					// OK OK bear with me here:
					// if is_hit[a] and is_hit[midpoint] then our boundry has to be between the midpoint between midpoint and b so replace a with the midpoint and run again
					// if !is_hit[a] and is_hit[midpoint] then our boundry must be between the midpoint and b so replace b with the midpoint and run again
				}
				else
				{
					a = !a_hit ? midpoint : a;
					b = a_hit ? midpoint : b;
					// same principle applies here as above ^ just the opposite
				}
			}


			if (a_hit)
			{

				Debug.Log("vertex added");
				vertex_positions.Insert(1, midpoint);
				vertex_positions.Insert(2, transform.position + (Vector3.Lerp(midpoint, b, 0.01f) - transform.position).normalized * Radius);
				Debug.DrawLine(transform.position, midpoint, Color.blue);

			}
			else
			{

				Debug.Log("vertex added");

				vertex_positions.Insert(1, transform.position + (Vector3.Lerp(midpoint, b, 0.01f) - transform.position).normalized * Radius);
				vertex_positions.Insert(2, midpoint);
				Debug.DrawLine(transform.position, midpoint, Color.blue);

			}
		}





		Vector3[] Vertices = new Vector3[vertex_positions.Count + 1];
		Vector3[] Normals = new Vector3[vertex_positions.Count + 1];


		Vertices[0] = Vector3.zero;

		Normals[0] = Vector3.down;
		for (int i = 1; i < Vertices.Length; i++)
		{

			Vertices[i] = transform.InverseTransformPoint(vertex_positions[i - 1]);
			Normals[i] = Vector3.down;
		}


		int[] Triangles = new int[3 * (Vertices.Length - 1)];

		for (int i = 0; i < Vertices.Length - 1; i++)
		{
			Triangles[i * 3] = 0;
			Triangles[(i * 3) + 1] = i + 1;
			Triangles[(i * 3) + 2] = i + 2;
			//makes Triangles based on the order of vertices
		}
		Triangles[3 * (Vertices.Length - 1) - 1] = 1;




		mesh.vertices = Vertices;
		mesh.triangles = Triangles;
		mesh.normals = Normals;
		mf.mesh = mesh;



	}
}
