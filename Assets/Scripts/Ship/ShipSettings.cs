﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipSettings : MonoBehaviour
{
    public float ship_health = 100.0f;
    public int broken_systems = 0;

    public Image health_bar;
    
    void Update()
    {
        ship_health -= broken_systems * Time.deltaTime;

        health_bar.fillAmount = ship_health / 100.0f;
        if (ship_health > 50.0f)
        {
            health_bar.color = Color.green;
        }
        if(ship_health <= 50.0f && ship_health > 20.0f)
        {
            health_bar.color = Color.yellow;
        }
        else if(ship_health <= 20.0f)
        {
            health_bar.color = Color.red;
        }

        if(ship_health <= 0)
        {
            Debug.Log("SHIP HAS DIED");
        }
    }
}
