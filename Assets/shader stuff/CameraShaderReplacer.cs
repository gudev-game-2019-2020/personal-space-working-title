﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaderReplacer : MonoBehaviour
{
	// Start is called before the first frame update
	private Camera cc;
	public Shader hidden;
    void Start()
    {
		cc = GetComponent<Camera>();

		cc.SetReplacementShader(hidden, "RenderType");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
